# No.1
Berikut merupakan program cloningan yang saya buat menggunakan beberapa algoritma yang mencakup logika permainan, mekanika permainan, dan interaksi pengguna/player. Algoritma tersebut dirancang untuk memungkinkan pengguna/player mendaftar, login, dan melakukan beberapa tindakan terkait informasi dalam in game berupa informasi pengguna secara realtime, upgrade building/bangunan, upgrade troop/pasukan serta informasi terkait hal yang berkenaan dengan upgrade baik itu bangunan/building juga pasukan/troop. Program ini saya implementasikan menggunakan pernyataan switch-case untuk memilih tindakan berdasarkan input pengguna dan perulangan if, else dan while untuk menjaga pengguna tetap berada dalam menu(Interface) hingga mereka memilih untuk keluar.

Berikut merupakan contoh Algoritma dalam program berupa pseudecode:

interface ClashOfClans {
    void login();
    void signUp();
    void playcoc();
}

class ClashOfClansUI implements ClashOfClans {
    private String username;
    private String password;
    private Player player;
    
    method login():
        print "== Clash of Clans - Login =="
        
        print "Username: "
        read username from user input
        
        print "Password: "
        read password from user input
        
        // Proses autentikasi login
        // ...
        
        player = new Player(username)
        
        print "Login successful. Welcome, " + username + "!"
    
    method signUp():
        print "== Clash of Clans - Sign Up =="
        
        print "Enter a username: "
        read username from user input
        
        print "Enter a password: "
        read password from user input
        
        // Proses registrasi pengguna
        // ...
        
        player = new Player(username)
        
        print "Sign up successful. Welcome, " + username + "!"
    
    method playcoc():
        if username is null or password is null:
            print "Please login first."
            return
        
        print "== Clash of Clans - Play coc =="
        print "Welcome back, " + username + "! Let's start playing!"
        
        // Membuat objek player
        player = new Player(username)

        cocRunning = true

        while cocRunning:
            print "1. Attack"
            print "2. Upgrade Building"
            print "3. Show Player Info"
            print "4. Upgrade Troops"
            print "0. Exit coc"
            
            print "Enter your choice: "
            read choice from user input
            
            switch choice:
                case 1:
                    player.attack()
                    break
                case 2:
                    player.upgradeBuilding()
                    break
                case 3:
                    player.showInfo()
                    break
                case 4:
                    player.upgradeTroops()
                    break
                case 0:
                    print "Thanks For Playing Clash Of Clans. Goodbye!"
                    cocRunning = false
                    break
                default:
                    print "Invalid choice. Please try again."
                    break
        
class Player:
    private String username
    private int level
    private int gold
    private int elixir
    private int troopLevel
    private boolean isBuildingDestroyed
    private int buildingLevel
    private Building target
    private Building building
    
    constructor Player(username):
        this.username = username
        level = 1
        gold = 1000
        elixir = 500
        troopLevel = 1
    
    method upgradeTroops():
        if troopLevel >= 5:
            print "Cannot upgrade troop. Troop is already at maximum level."
            return
        
        troopLevel++
        print "Troop has been upgraded to level " + troopLevel + "."
    
    method attack():
        print "Player " + username + " is attacking..."
        
        // Mendapatkan pasukan yang tersedia
        troops = getAvailableTroops()
        
        // Memilih target untuk diserang
        target = selectTarget()
        
        // Menyerang target dengan pasukan yang tersedia
        for troop in troops:
            troop.attack(target)
        
        print "Attack complete."
    
    method getAvailableTroops():
        // Logika untuk mendapatkan pasukan yang tersedia
        troops = new Troop[3]
        
        troops[0] = new Troop("Archer", 5)
        troops[1] = new Troop("Barbarian", 4)
        troops[2] = new Troop("Giant", 3)
        
        return troops
    
    method selectTarget():
        // Daftar building yang ada
        buildings = [
            new Building("Town Hall"),
            new Building("Archer Tower"),
            new Building("Cannon"),
            new Building("Mortar")
        ]
        
        // Memilih target secara acak dari daftar building
        random = new Random()
        index = random.nextInt(buildings.length)
        target = buildings[index]
        
        print "Selected target: " + target.getName()
        
        return target
    
    method upgradeBuilding():
        print "Player " + username + " is upgrading building..."
        if isBuildingDestroyed:
            print "Cannot upgrade building. Building is destroyed."
            return
            
        if buildingLevel >= 10:
            print "Cannot upgrade building. Building is already at maximum level."
            return
            
        buildingLevel++
        print "Building has been upgraded to level " + buildingLevel + "."
    
    method destroyBuilding():
        if isBuildingDestroyed:
            print "Building is already destroyed."
            return
        
        // Proses penghancuran bangunan
        // ...
        
        isBuildingDestroyed = true
        print "Building has been destroyed."
    
    method showInfo():
        print "== Player Info =="
        print "Username: " + username
        print "Level: " + level
        print "Gold: " + gold
        print "Elixir: " + elixir
        // Informasi tambahan tentang pemain
    
class Troop:
    private String name
    private int level
    
    constructor Troop(name, level):
        this.name = name
        this.level = level
    
    method attack(target):
        print "Troop " + name + " is attacking building..."
        
        // Menghasilkan jumlah damage yang akan diberikan ke target
        totalDamage = calculateDamage()
        
        // Menyerang target dengan jumlah damage yang dihasilkan
        target.takeDamage(totalDamage)
        
        print "Troop attack complete."
    
    method calculateDamage():
        // Logika perhitungan damage
        // ...
        return 0
    
class Building:
    private String name
    private int level
    private int health
    private boolean isDestroyed
    
    constructor Building(name):
        this.name = name
        level = 1
        health = 100
    
    method takeDamage(damage):
        health -= damage
        print "Building " + name + " takes " + damage + " damage."
        
        if health <= 0:
            destroy()
    
    method destroy():
        isDestroyed = true
        print "Building " + name + " has been destroyed."
    
main():
    scanner = new Scanner(System.in)
    cocUI = new ClashOfClansUI()
        
    // Menu utama
    while true:
        print "== Clash of Clans =="
        print "1. Login"
        print "2. Sign Up"
        print "3. Play Game"
        print "0. Exit"
        
        print "Enter your choice: "
        choice = scanner.nextInt()
        
        switch choice:
            case 1:
                cocUI.login()
                break
            case 2:
                cocUI.signUp()
                break
            case 3:
                cocUI.playcoc()
                break
            case 0:
                print "Exiting Game. Goodbye!"
                break
            default:
                print "Invalid choice. Please try again."
                break

# No.2
Penjelasan dari Algoritma yang telah dibuat:

Berikut adalah penjelasan lebih rinci tentang algoritma dalam program:
1. Ada tiga entitas utama dalam program ini, yaitu antarmuka ClashOfClans, implementasi ClashOfClansUI, dan kelas Player. ClashOfClansUI mengimplementasikan antarmuka ClashOfClans dan memiliki metode login(), signUp(), dan playcoc(). Player adalah kelas yang merepresentasikan pemain dalam permainan Clash of Clans dan memiliki metode-metode terkait seperti upgradeTroops(), attack(), getAvailableTroops(), dan selectTarget().

2. Pada saat dijalankan, program akan menampilkan menu utama berupa pilihan untuk login, sign up, atau bermain. Pilihan tersebut akan diambil dari input pengguna.

3. Jika pengguna memilih untuk login, program akan meminta pengguna memasukkan username dan password. Proses autentikasi login akan dilakukan (tidak dijelaskan secara rinci dalam program ini). Jika login berhasil, objek Player akan dibuat dengan menggunakan username yang dimasukkan.

4. Jika pengguna memilih untuk sign up, program akan meminta pengguna memasukkan username dan password. Proses registrasi pengguna akan dilakukan (tidak dijelaskan secara rinci dalam program ini). Jika registrasi berhasil, objek Player akan dibuat dengan menggunakan username yang dimasukkan.

5. Jika pengguna memilih untuk bermain (playcoc), program akan memeriksa apakah username dan password sudah ada atau belum. Jika belum ada, program akan menampilkan pesan "Please login first." Jika sudah ada, program akan membuat objek Player dengan menggunakan username yang dimasukkan.

6. Setelah berhasil login atau sign up, program akan memasuki loop permainan. Dalam loop ini, program akan menampilkan pilihan menu berupa attack, upgrade building, show player info, upgrade troops, atau exit. Pilihan tersebut akan diambil dari input pengguna.

7. Jika pengguna memilih attack, program akan memanggil metode attack() pada objek Player. Metode ini akan melakukan serangan pemain terhadap target menggunakan pasukan yang tersedia. Pasukan yang tersedia diperoleh dari metode getAvailableTroops(), sedangkan target yang akan diserang diperoleh dari metode selectTarget(). Setiap pasukan akan melakukan serangan ke target dengan memanggil metode attack() pada objek Troop.

8. Jika pengguna memilih upgrade building, program akan memanggil metode upgradeBuilding() pada objek Player. Metode ini akan meningkatkan level bangunan jika bangunan belum hancur dan belum mencapai level maksimum.

9. Jika pengguna memilih show player info, program akan memanggil metode showInfo() pada objek Player. Metode ini akan menampilkan informasi tentang pemain seperti username, level, gold, elixir, dan informasi tambahan lainnya.

10. Jika pengguna memilih upgrade troops, program akan memanggil metode upgradeTroops() pada objek Player. Metode ini akan meningkatkan level pasukan jika pasukan belum mencapai level maksimum.

11. Jika pengguna memilih exit, program akan keluar dari loop permainan dan menampilkan pesan "Thanks For Playing Clash Of Clans. Goodbye!".

12. Program akan terus berjalan di menu utama hingga pengguna memilih untuk keluar dengan memilih exit pada pilihan menu.

Berikut source code yang dibuat sebagai implementasi algoritma pada class ClashOfClans:

[ClashOfClans](Clash_Of_Clans.java)

# No.3
Konsep dasar OOP (Object-Oriented Programming) adalah pendekatan dalam pemrograman yang menggunakan objek sebagai unit utama untuk memodelkan dan memecahkan masalah. Program yang saya buat merupakan contoh implementasi OOP dalam bahasa pemrograman Java. Berikut adalah penjelasan singkat mengenai class, atribut, metode, objek, dan properti yang ada dalam program ClashOfClans:

**Pada program yang saya buat, terdapat beberapa class, metode, atribut, objek, dan properti yang dapat dijelaskan sebagai berikut:**
a. Class: Dalam program tersebut, terdapat beberapa class yaitu ClashOfClansUI, Player, Troop, dan Building. Class digunakan untuk merepresentasikan sebuah konsep atau objek tertentu dan mendefinisikan struktur dan perilaku yang dimiliki oleh objek-objek yang berasal dari class tersebut.

b. Atribut: Atribut adalah variabel yang dideklarasikan di dalam sebuah class dan digunakan untuk menyimpan data atau informasi yang terkait dengan objek dari class tersebut. Contoh atribut dalam program tersebut antara lain username, password, level, gold, elixir, troopLevel, dan buildingLevel.

c. Metode: Metode adalah blok kode yang terdapat di dalam sebuah class dan digunakan untuk menjalankan tugas atau operasi tertentu. Metode dapat menerima input, memproses data, dan menghasilkan output. Dalam program tersebut, terdapat beberapa metode seperti login(), signUp(), playcoc(), upgradeTroops(), attack(), getAvailableTroops(), selectTarget(), upgradeBuilding(), destroyBuilding(), showInfo(), attack(), calculateDamage(), takeDamage(), dan destroy().

d. Objek: Objek adalah representasi konkret dari sebuah class. Objek dibuat berdasarkan blueprint class dan memiliki atribut serta dapat melakukan operasi yang didefinisikan dalam class tersebut. Dalam program tersebut, terdapat objek-objek yang dibuat, seperti objek cocUI yang merupakan instance dari class ClashOfClansUI dan objek player yang merupakan instance dari class Player.

e. Properti: Properti adalah atribut yang dimiliki oleh sebuah objek. Properti menggambarkan keadaan atau karakteristik objek tersebut. Dalam program tersebut, contoh properti adalah username, level, gold, elixir, dan buildingLevel yang merupakan atribut dari objek player.

Dengan menggunakan class, atribut, metode, objek, dan properti, program dapat mengatur data dan perilaku objek-objek yang berinteraksi dalam permainan Clash of Clans.

**Konsep OOP melibatkan empat prinsip utama: Encapsulation, Inheritance, Polymorphism, dan Abstraction.**
1. Encapsulation (Enkapsulasi): Pilar ini terlihat dalam penggunaan access modifiers (misalnya private, protected) untuk mengatur akses ke atribut dan metode dalam kelas-kelas tersebut. Contohnya, atribut username, level, gold, dan elixir pada kelas Player menggunakan access modifier protected untuk membatasi akses dari kelas turunan.

2. Inheritance (Pewarisan): Pilar ini diimplementasikan dengan menggunakan konsep pewarisan kelas. Kelas PlayerPremium merupakan turunan dari kelas Player, sehingga mewarisi atribut dan metode dari kelas Player. Pilar ini memungkinkan kita untuk membuat hierarki kelas, di mana kelas turunan dapat mewarisi sifat-sifat (atribut dan metode) dari kelas induknya.

3. Polymorphism (Polimorfisme): Pilar ini terlihat dalam penggunaan method overriding pada metode upgradeTroops() di kelas PlayerPremium. Dalam kelas turunan PlayerPremium, metode upgradeTroops() didefinisikan ulang dengan implementasi yang berbeda dari metode yang sama di kelas induk Player. Polimorfisme memungkinkan kita untuk menggunakan objek kelas turunan secara generik melalui referensi kelas induk.

4. Abstraction (Abstraksi): Pilar ini terlihat dalam penggunaan antarmuka (interface) ClashOfClans. Antarmuka ini mendefinisikan kontrak untuk metode-metode yang harus ada dalam implementasinya. Kelas ClashOfClansUI mengimplementasikan antarmuka ini dan memberikan implementasi konkret untuk metode-metode tersebut. Pilar ini memungkinkan kita untuk menyembunyikan detail implementasi dan hanya fokus pada fungsionalitas yang diperlukan.

Dengan menerapkan pewarisan dan menggunakan pilar-pilar OOP ini, kita dapat memperluas dan mengatur kode dengan lebih terstruktur, meningkatkan reusabilitas, dan memudahkan pengembangan program secara keseluruhan.

Keuntungan OOP termasuk modularitas (memudahkan pengelolaan dan perbaikan program), reusabilitas (menghemat waktu dan upaya pengembangan), skaalabilitas (dapat diperluas dan dimodifikasi dengan mudah), pemahaman yang lebih baik (fokus pada konsep yang relevan), dan pemecahan masalah yang intuitif (sesuai dengan cara berpikir manusia dan dunia nyata).

# No.4
Encapsulation (Enkapsulasi) mengacu pada konsep menyembunyikan rincian implementasi dari objek dan hanya memperlihatkan fungsi-fungsi publik yang dapat diakses oleh pengguna. Pada contoh program yang saya buat, atribut username, level, gold, elixir, troopLevel pada class Player, serta atribut name, level, health pada class Building diubah menjadi privat. Kemudian, metode getter dan setter disediakan untuk masing-masing atribut private sehingga hanya dapat mengakses dan mengubah nilai atribut melalui metode tersebut.

Dalam program yang saya buat, enkapsulasi diimplementasikan untuk menyembunyikan rincian implementasi yang sensitif atau tidak relevan dari objek-objek yang terlibat. Enkapsulasi memungkinkan pengguna program hanya berinteraksi dengan antarmuka publik yang ditentukan oleh class-class tersebut.

Berikut adalah contoh implementasi enkapsulasi dalam program tersebut:
1. Pada kelas Player, atribut username, level, gold, elixir, dan troopLevel dienkapsulasi dengan menggunakan akses modifier private. Hal ini berarti atribut-atribut tersebut tidak dapat diakses langsung dari luar kelas. Untuk mengakses atau mengubah nilainya, kita harus menggunakan metode-metode publik yang telah disediakan, seperti getUsername(), getLevel(), setGold(), dan sebagainya. Dengan demikian, enkapsulasi memastikan bahwa akses dan perubahan data dilakukan melalui metode yang telah ditentukan, sehingga dapat mengontrol dan memvalidasi nilai yang dioperasikan.

2. Pada kelas Troop, atribut name dan level juga dienkapsulasi dengan akses modifier private. Untuk mendapatkan nilai atribut atau melakukan operasi terhadap atribut tersebut, kelas Troop menyediakan metode-metode publik seperti getName() dan upgrade(). Dengan demikian, enkapsulasi membatasi akses langsung ke atribut-atribut internal kelas dan mendorong penggunaan metode-metode yang telah ditentukan untuk berinteraksi dengan atribut.

3. Selain itu, terdapat juga enkapsulasi pada kelas Building. Atribut-atribut seperti name, level, health, dan isDestroyed dienkapsulasi dengan menggunakan akses modifier private. Kelas ini juga menyediakan metode-metode publik seperti getName(), getLevel(), takeDamage(), dan destroy() untuk mengakses dan memanipulasi atribut-atribut tersebut. Dengan enkapsulasi ini, kita dapat membatasi akses langsung ke atribut-atribut internal kelas dan menjaga integritas data dengan memvalidasi perubahan melalui metode yang disediakan.

Dengan menggunakan encapsulation, kita dapat mengatur aksesibilitas dan memastikan bahwa atribut hanya dapat diakses atau diubah melalui metode yang telah ditentukan. Hal ini membantu dalam menjaga keamanan dan konsistensi data dalam program, serta memungkinkan adanya validasi dan logika tambahan pada setiap akses atau perubahan nilai atribut.

Berikut source code yang diimplementasikan dalam encapsulation terdapat pada class Player dan class Building.

[ClashOfClans](Clash_Of_Clans.java)

# No.5
Abstraction (Abstraksi) mengacu pada proses menyederhanakan kompleksitas dengan menyembunyikan detail yang tidak relevan dari pengguna. Pada contoh program yang saya buat, terdapat class abstrak Building yang memiliki atribut dan metode umum untuk bangunan dalam permainan Clash of Clans. Terdapat pula dua kelas turunan, yaitu TownHall dan Cannon, yang mewarisi kelas Building dan mengimplementasikan metode abstrak yang didefinisikan di dalamnya.

Dalam program yang saya buat, abstraksi digunakan untuk menyembunyikan kompleksitas rincian implementasi yang tidak relevan dan fokus pada fitur dan fungsi yang penting dari objek-objek yang terlibat. Abstraksi membantu pengguna program untuk lebih mudah memahami dan menggunakan objek-objek tersebut dengan menyediakan tingkat pemahaman yang lebih tinggi melalui antarmuka yang lebih sederhana.

Berikut adalah contoh implementasi abstraksi dalam program tersebut:
1. Terdapat kelas abstrak Building yang berfungsi sebagai kerangka kerja umum untuk semua jenis bangunan dalam permainan. Kelas ini memiliki atribut dan metode yang umum untuk semua bangunan, seperti name (nama bangunan), level (tingkat bangunan), dan health (kesehatan bangunan). Selain itu, terdapat pula metode abstrak seperti upgrade() dan destroy(), yang harus diimplementasikan oleh kelas turunan.

2. Terdapat kelas turunan seperti TownHall dan Cannon yang mewarisi kelas Building dan memberikan implementasi khusus untuk metode abstrak yang ada. Misalnya, kelas TownHall mengimplementasikan metode upgrade() dengan logika untuk meningkatkan level Town Hall, sedangkan kelas Cannon mengimplementasikan metode upgrade() dengan logika untuk meningkatkan level Cannon.

Dengan menggunakan abstraction, kita dapat menyediakan kerangka kerja umum melalui kelas abstrak Building yang memiliki beberapa metode abstrak yang harus diimplementasikan oleh kelas turunan. Hal ini memungkinkan kita untuk menggeneralisasi perilaku dan atribut yang berkaitan dengan bangunan, sementara memungkinkan kelas turunan untuk memberikan implementasi khusus sesuai dengan jenis bangunan yang spesifik.

Berikut source code yang diimplementasikan dalam abstraction terdapat pada class SignUp, class Login, dan class Building.

[ClashOfClans](Clash_Of_Clans.java)

# No.6
Dalam program yang saya buat, terdapat konsep pewarisan (inheritance) dan polimorfisme (polymorphism) yang digunakan untuk membangun hubungan antara class-class yang berbeda. Berikut adalah penjelasan lebih detail tentang kedua konsep tersebut:

1. Pewarisan (Inheritance)
Inheritance adalah salah satu konsep dalam pemrograman berorientasi objek (OOP) yang memungkinkan pembuatan hierarki class, di mana class yang baru dibuat dapat mewarisi sifat-sifat (atribut dan metode) dari class yang sudah ada. Class yang sudah ada disebut kelas induk atau superclass, sedangkan class yang mewarisi sifat-sifat tersebut disebut class anak atau subclass. Dalam konteks program Clash of Clans, kita dapat mengimplementasikan inheritance dengan membuat class yang mewarisi sifat-sifat dari class yang lebih umum ke class yang lebih spesifik.

Contoh implementasi inheritance pada program Clash of Clans adalah sebagai berikut:
- Terdapat class DefenseBuilding dan ResourceBuilding merupakan subclass dari class Building. Subclass akan mewarisi atribut dan metode dari superclass, sehingga tidak perlu menulis ulang kode yang sama. Misalnya, jika class Building memiliki atribut name, maka atribut tersebut juga akan tersedia di kelas DefenseBuilding dan ResourceBuilding tanpa perlu mendefinisikannya ulang.

2. Polimorfisme (Polymorphism)
Polimorfisme adalah konsep dalam OOP yang memungkinkan objek memiliki banyak bentuk atau tipe. Dengan polimorfisme, objek dari kelas yang berbeda dapat diproses menggunakan metode yang sama, dan metode tersebut akan berperilaku berbeda tergantung pada jenis objek yang diproses. Polimorfisme dapat dicapai melalui penggunaan konsep overriding (penggantian metode dalam kelas anak) atau konsep overloading (membuat metode dengan nama yang sama tetapi dengan parameter yang berbeda).

Contoh implementasi polimorfisme pada program Clash of Clans adalah sebagai berikut:
- Terdapat class Troop sebagai superclass dan kelas Archer serta Barbarian sebagai subclass. Setiap class memiliki metode attack(), namun masing-masing subclass mengimplementasikan metode tersebut sesuai dengan perilaku serangan yang spesifik. Ketika memanggil metode attack() pada objek Troop, metode yang akan dipanggil akan bergantung pada tipe objek yang sebenarnya saat runtime. Sebagai contoh, jika kita memiliki objek Archer dan memanggil metode attack(), maka metode yang dipanggil akan mengikuti implementasi serangan Archer. Hal ini memungkinkan adanya variasi perilaku yang sesuai dengan jenis objek yang sedang diproses.

Program Clash of Clans yang saya buat ini meskipun hanya berupa clone dan belum terimplementasi semuanya. Namun, penggunaan keempat pilar OOP (salah duanya Inheritance dan Polimorphisme) dapat dibangun dengan struktur yang hierarkis dan memungkinkan objek-objek dengan sifat dan perilaku yang berbeda dapat dikelola secara efisien dan fleksibel.

Berikut source code yang diimplementasikan dalam Inheritance dan Polymorphism

[ClashOfClans](Clash_Of_Clans.java)

# No.7
Cara mendeskripsikan proses bisnis (kumpulan use case) ke dalam skema OOP dalam program Clash Of Clans:

Berikut adalah penjelasan lebih detail mengenai implementasi proses bisnis ke dalam skema OOP dalam program Clash Of Clans:

1. Identifikasi objek:
- User: Mewakili pengguna atau pemain game Clash of Clans.
- Supercell: Mewakili pengembang game Clash of Clans.
- Desa: Mewakili desa atau base milik pengguna.
- Bangunan: Mewakili bangunan yang dapat dibangun dalam desa.
- Pasukan: Mewakili pasukan yang dapat dilatih dan digunakan dalam serangan.
- Pembelian: Mewakili pembelian in-game dan top-up.
- Clan: Mewakili kelompok pemain yang berada dalam satu kelompok.
- Perang Clan: Mewakili pertempuran antara kelompok Clan.

2. Identifikasi hubungan antar objek:
- User dapat mengatur posisi/kondisi desa secara realtime.
- User dapat mendapatkan informasi dari Supercell secara realtime.
- User dapat mengakses berita, acara, komunitas, dan scene e-sport dalam game.
- User dapat melakukan pembelian in-game dan top-up.
- User dapat membeli dekorasi, termasuk hero/pahlawan, dengan harga yang lengkap dalam game.
- User dapat menyesuaikan pengaturan game pada menu settings dalam game secara realtime.
- User dapat mengganti akun dalam game secara leluasa.
- User dapat mengatur format desa dan format base yang akan digunakan.
- User dapat melihat detail pembelian bangunan dan rincian harga.
- User dapat melihat jumlah dan detail sumber daya seperti permata, Dark Elixir, Elixir, dan Gold.
- User dapat melihat dan mengatur durasi shield desa dari serangan musuh.
- User dapat melihat informasi terkait Clash of Clans seperti kebijakan privasi, persyaratan layanan, dan lainnya.
- User dapat melihat profil user, desa asal, basis pembangun, ibu kota klan, dan informasi lainnya.
- User dapat melihat clan yang sedang disinggahi, rekomendasi clan lain, dan interaksi sosial dalam game.
- User dapat melihat profil dan informasi anggota clan, serta melakukan komunikasi dalam clan.
- User dapat melihat dan mengakses riwayat pertahanan desa dan penyerangan user lain.
- User dapat melihat informasi liga/ranking, rentetan clan, pemain/pengguna teratas, dan perang liga.
- User dapat melakukan penyerangan terhadap desa milik user lain, baik dalam mode Practice, Single Player, atau Multiplayer.
- User dapat mengakses informasi tentang pasukan, ramuan, alat tempur, dan estimasi waktu selesai latihan.
- User dapat melihat profil musuh lain, harta rampasan perang, dan informasi lainnya.
- User dapat melihat progress dan menyelesaikan misi-misi yang disediakan dalam game.
- User dapat melakukan registrasi akun dan mengetahui informasi server maintenance.
- Manajemen COC dapat memantau transaksi out-game dan memfilter kondisi user dalam clan.

3. Implementasikan objek dan hubungan ke dalam struktur OOP:
- Buat kelas-kelas yang merepresentasikan objek-objek yang diidentifikasi sebelumnya, seperti kelas User, Desa, Bangunan, Pasukan, Pembelian, Clan, dan lainnya.
- Definisikan atribut-atribut yang sesuai dengan data yang diperlukan untuk setiap kelas.
- Definisikan metode-metode yang merepresentasikan aksi atau perilaku yang dapat dilakukan oleh objek-objek tersebut.
- Hubungkan objek-objek tersebut dengan cara menerapkan asosiasi, komposisi, atau agregasi sesuai dengan hubungan antar objek yang telah diidentifikasi.
- Pastikan bahwa struktur kelas-kelas dan hubungannya menggambarkan dengan akurat proses bisnis yang dijelaskan dalam tabel use case Clash of Clans.

Dengan menggunakan skema OOP seperti di atas, program Clash Of Clans menjadi lebih terstruktur, modular, dan mudah dimengerti. Setiap entitas dalam proses bisnis direpresentasikan oleh class-class terpisah yang memiliki tanggung jawab spesifik. Keuntungan dari pendekatan ini antara lain:

1. Modularitas: Proses bisnis yang kompleks dibagi menjadi class-class yang terpisah, sehingga setiap class bertanggung jawab hanya pada tugas tertentu. Hal ini memudahkan pemeliharaan dan pengembangan program di masa depan.

2. Reusabilitas: Dengan menerapkan konsep inheritance, beberapa class seperti ResourceBuilding dapat mewarisi sifat dan fungsi dari class induk Building. Hal ini mengurangi duplikasi kode dan memungkinkan penggunaan ulang yang efisien.

3. Polymorphism: Melalui konsep polymorphism, objek-objek yang memiliki hierarki class yang sama, seperti DefenseBuilding dan Building, dapat diperlakukan secara seragam dan digunakan secara interchangeable. Misalnya, objek DefenseBuilding dapat digunakan di tempat objek Building jika dibutuhkan.

4. Abstraksi: Setiap class merepresentasikan entitas nyata dalam proses bisnis. Informasi dan perilaku yang terkait dengan entitas tersebut diabstraksikan ke dalam class, sehingga memudahkan pemahaman dan manipulasi data terkait.

# No.8
Class Diagram

[Diagram Class](ClassDiagramCOC.png)

1. ClashOfClans
- Merupakan interface yang berisi tiga metode: login(), signUp(), dan playCoc().
- Interface ini berfungsi sebagai kontrak untuk kelas-kelas yang mengimplementasikannya.

2. ClashOfClansUI
- Implementasi dari interface ClashOfClans.
- Class ini bertanggung jawab untuk menampilkan antarmuka pengguna Clash of Clans dan menghubungkan pengguna dengan fitur-fitur permainan.
- Metode login() digunakan untuk meminta pengguna untuk melakukan login dengan memasukkan username dan password.
- Metode signUp() digunakan untuk meminta pengguna untuk mendaftar dengan membuat username dan password baru.
- Metode playCoc() digunakan untuk memulai permainan Clash of Clans setelah pengguna berhasil login atau mendaftar.

3. Player
- Representasi dari pemain dalam permainan Clash of Clans.
- Memiliki atribut seperti username, level, gold, elixir, darkelixir, troopLevel, isBuildingDestroyed, buildingLevel, target, dan building.
- Metode-metode dalam class Player mencakup:
    - Constructor: Untuk menginisialisasi objek Player dengan nilai awal yang tepat.
    - getUsername(): Mengembalikan username pemain.
    - attack(): Menggambarkan serangan pemain.
    - upgradeBuilding(): Menggambarkan peningkatan bangunan.
    - showInfo(): Menampilkan informasi pemain.
    - upgradeTroops(): Menggambarkan peningkatan pasukan.

4. Troop
- Representasi dari pasukan dalam permainan Clash of Clans.
- Kelas ini mungkin tidak terlihat dalam use case diagram, tetapi disebutkan dalam metode upgradeTroops() dalam class Player.
- Kelas ini dapat memiliki atribut dan metode terkait dengan pasukan, seperti level pasukan, jenis pasukan, kekuatan serangan, dan sebagainya.

5. Building
- Representasi dari bangunan dalam permainan Clash of Clans.
- Kelas ini mungkin tidak terlihat dalam use case diagram, tetapi disebutkan dalam metode upgradeBuilding() dalam class Player.
- Kelas ini dapat memiliki atribut dan metode terkait dengan bangunan, seperti level bangunan, jenis bangunan, kesehatan bangunan, dan sebagainya.

Use Case Table: [Use Case](Use_Case_Table.docx)

# No.9
[Link Video Demonstrasi](https://youtu.be/3UU-jVATyqk)

# No.10
[ScreenShot Program 1](uts1.png), [ScreenShot Program 2](uts2.png), [ScreenShot Program 3](uts3.png),
[ScreenShot Program 4](uts4.png), [ScreenShot Program 5](uts5.png), [ScreenShot Program 6](uts6.png),
[ScreenShot Program 7](uts7.png), [ScreenShot Program 8](uts8.png).
