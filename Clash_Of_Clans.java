/**
 * Program ini adalah implementasi sederhana dari permainan Clash of Clans menggunakan konsep OOP.
 * Program ini terdiri dari beberapa kelas yang saling berinteraksi untuk melakukan operasi seperti login,
 * sign up, upgrade bangunan, menyerang, dan menampilkan informasi player.
 */
import java.util.Random;
import java.util.Scanner;

// Interface ClashOfClans digunakan sebagai kontrak yang harus diimplementasikan oleh kelas ClashOfClansUI
public interface ClashOfClans {
    void login(); // Metode untuk login
    void signUp(); // Metode untuk sign up
    void playCoc(); // Metode untuk memainkan permainan Clash of Clans
}

// Inheritance
// Class ClashOfClansUI yang mengimplementasikan interface ClashOfClans
class ClashOfClansUI implements ClashOfClans {
    // Encapsulation
    private Player player;
    
    @Override
    // Encapsulation
    public void login() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("== Clash of Clans - Login ==");
        
        System.out.print("Masukkan Username: ");
        String username = scanner.nextLine();

        System.out.print("Masukkan Password: ");
        String password = scanner.nextLine();
        
        // Proses autentikasi login
        player = new Player(username);
        
        System.out.println("Login Berhasil :). Selamat Datang, " + player.getUsername() + "!");
    }
    
    @Override
    // Encapsulation
    public void signUp() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("== Clash of Clans - Sign Up ==");

        System.out.print("Silahkan Registrasi Username Anda: ");
        String username = scanner.nextLine();

        System.out.print("Masukkan Password Anda: ");
        String password = scanner.nextLine();
        
        // Proses registrasi pengguna
        player = new Player(username);
        
        System.out.println("Sign up Berhasil :). Selamat Bergabung, " + player.getUsername() + "!");
    }
    
    @Override
    // Encapsulation
    public void playCoc() {
        if (player == null) {
            System.out.println("Lakukan Sign Up atau Login Terlebih Dahulu!!!.");
            return;
        }

        System.out.println();
        System.out.println("== Clash of Clans - Main Clash Of Clans ==");
        System.out.println("Selamat Datang Kembali, " + player.getUsername() + "! Mari Mulai Permainannya!");

        Scanner scanner = new Scanner(System.in);
        boolean cocRunning = true;

        while (cocRunning) {
            System.out.println("!!! ===== Clash Of Clans ===== !!!");
            System.out.println("!! ======= In Game Menu ======= !!");
            System.out.println("1. Serang");
            System.out.println("2. Tingkatkan Bangunan");
            System.out.println("3. Tampilkan Informasi Player");
            System.out.println("4. Tingkatkan Pasukan");
            System.out.println("0. Keluar Game");

            System.out.print("Masukkan Pilihanmu: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    player.attack();
                    break;
                case 2:
                    // Polymorphism
                    player.upgradeBuilding();
                    break;
                case 3:
                    player.showInfo();
                    break;
                case 4:
                    // Polymorphism
                    player.upgradeTroops();
                    break;
                case 0:
                    System.out.println("Terima Kasih Telah Bermain Clash Of Clans. Sampai Berjumpa lagi :)");
                    cocRunning = false;
                    break;
                default:
                    System.out.println("Pilihan Tidak Tersedia!. Silahkan Coba Lagi.");
                    break;
            }

            System.out.println();
        }
    }
}

// Abstraction
// Class Player yang merepresentasikan data dan operasi yang dapat dilakukan oleh player
class Player {
    // Encapsulation
    private String username;
    private int level;
    private int gold;
    private int elixir;
    private int darkelixir;
    private int troopLevel;
    private boolean isBuildingDestroyed;
    private int buildingLevel;
    private Building target;
    private Building building;
    
    // Constructor untuk membuat objek Player dengan username tertentu
    public Player(String username) {
        this.username = username;
        level = 1;
        gold = 2000000;
        elixir = 2500000;
        darkelixir = 20000;
        troopLevel = 1;
        buildingLevel = 0;
    }
    
    // Encapsulation
    // Getter untuk mengambil username player
    public String getUsername() {
        return username;
    }
    
    // Encapsulation dan Polymorphism
    // Method untuk meningkatkan level pasukan
    public void upgradeTroops() {
        if (troopLevel >= 5) {
            System.out.println("Tidak Dapat Meningkatkan Pasukan! Pasukan telah berada di level maksimal.");
            return;
        }
        
        troopLevel++;
        System.out.println("Pasukan telah berhasil ditingkatkan ke level " + troopLevel + ".");
    }

    // Encapsulation dan Polymorphism
    // Method untuk melakukan serangan
    public void attack() {
        // Mendapatkan pasukan yang tersedia
        Troop[] troops = getAvailableTroops();
        
        // Memilih target untuk diserang
        System.out.println("Player " + username + " sedang menyerang...");

        Building target = selectTarget();
        
        // Menyerang target dengan pasukan yang tersedia
        for (Troop troop : troops) {
            troop.attack(target);
        }
        
        System.out.println("Penyerangan selesai.");
    }
    
    // Encapsulation
    // Method untuk mendapatkan pasukan yang tersedia
    private Troop[] getAvailableTroops() {
        // Logika untuk mendapatkan pasukan yang tersedia
        Troop[] troops = new Troop[9];
        
        troops[0] = new Troop("Archer", 1);
        troops[1] = new Troop("Barbarian", 1);
        troops[2] = new Troop("Giant", 2);
        troops[3] = new Troop("Wall Breaker", 2);
        troops[4] = new Troop("Wizard", 1);
        troops[5] = new Troop("Ballon", 1);
        troops[6] = new Troop("Goblin", 1);
        troops[7] = new Troop("PEKKA", 2);
        troops[8] = new Troop("Minion", 1);

        return troops;
    }
    
    // Encapsulation
    // Method untuk memilih target
    private Building selectTarget() {
        // Daftar building yang ada
        Building[] buildings = {
            new Building("Town Hall", 1),
            new Building("Archer Tower", 2),
            new Building("Wizard Tower", 1),
            new Building("Cannon", 2),
            new Building("Mortar", 1)
        };
        
        // Memilih target secara acak dari daftar building
        Random random = new Random();
        int index = random.nextInt(buildings.length);
        Building target = buildings[index];
        
        System.out.println("Selected target: " + target.getName());
        
        return target;
    }
    
    // Encapsulation dan Polymorphism
    // Method untuk meningkatkan level bangunan
    public void upgradeBuilding() {
        if (buildingLevel >= 10) {
            System.out.println("Tidak Dapat Meningkatkan Bangunan! Bangunan telah berada di level maksimal.");
            return;
        }
            
        buildingLevel++;
        System.out.println("Bangunan telah ditingkatkan ke level " + buildingLevel + ".");
    }
    
    // Encapsulation
    // Method untuk menghancurkan bangunan
    private void destroyBuilding() {
        if (isBuildingDestroyed) {
            System.out.println("Bangunan ini sudah dihancurkan.");
            return;
        }
        
        // Proses penghancuran bangunan
        // ...
        
        isBuildingDestroyed = true;
        System.out.println("Bangunan telah dihancurkan.");
    }
    
    // Encapsulation
    // Method untuk menampilkan informasi player
    public void showInfo() {
        System.out.println("== Informasi Player ==");
        System.out.println("Username: " + username);
        System.out.println("Level: " + level);
        System.out.println("Maksimum Gold: 3000000");
        System.out.println("Gold: " + gold);
        System.out.println("Maksimum Elixir: 3000000");
        System.out.println("Elixir: " + elixir);
        System.out.println("Maksimum Dark Elixir: 30000");
        System.out.println("Dark Elixir: " + darkelixir);
        System.out.println("Level Pasukan: " + troopLevel);
        System.out.println("Level Bangunan: " + buildingLevel);
    }
}

// Abstraction
// Class Troop yang merepresentasikan pasukan dalam permainan
class Troop {
    // Encapsulation
    private String name;
    private int level;
    
    // Constructor untuk membuat objek Troop dengan nama dan level tertentu
    public Troop(String name, int level) {
        this.name = name;
        this.level = level;
    }
    
    // Encapsulation dan Polymorphism
    // Method untuk melakukan serangan ke bangunan target
    public void attack(Building target) {
        System.out.println("Pasukan " + name + " sedang menyerang bangunan...");
        
        // Menghasilkan jumlah damage yang akan diberikan ke target
        int totalDamage = calculateDamage();
        
        // Menyerang target dengan jumlah damage yang dihasilkan
        target.takeDamage(totalDamage);
        
        System.out.println("Pasukan berhasil menyerang.");
    }
    
    // Encapsulation
    // Method untuk menghitung damage yang dihasilkan oleh pasukan
    private int calculateDamage() {
        // Logika perhitungan damage
        Random random = new Random();
        int baseDamage = level * 10;
        int randomFactor = random.nextInt(11) - 5; // Nilai random antara -5 sampai 5
        int totalDamage = baseDamage + randomFactor;
        
        return totalDamage;
    }

    public int getLevel() {
        return 0;
    }
}

// Abstraction
// Class Hero yang merepresentasikan hero dalam permainan
class Hero extends Troop {
    // Encapsulation
    private String specialAbility;
    
    // Constructor untuk membuat objek Hero dengan nama, level, dan specialAbility tertentu
    public Hero(String name, int level, String specialAbility) {
        super(name, level);
        this.specialAbility = specialAbility;
    }
    
    // Encapsulation dan Polymorphism
    // Method untuk melakukan serangan khusus hero ke bangunan target
    public void useSpecialAbility(Building target) {
        System.out.println("Hero " + getName() + " menggunakan special ability...");
        
        // Menghasilkan jumlah damage khusus yang akan diberikan ke target
        int totalDamage = calculateSpecialDamage();
        
        // Menyerang target dengan jumlah damage khusus yang dihasilkan
        target.takeDamage(totalDamage);
        
        System.out.println("Hero berhasil menggunakan special ability.");
    }

    private Hero[] getName() {
        //Logika untuk menurunkan Hero yang tersedia
        Hero[] hero = new Hero[2];

        hero[0] = new Hero("Barbarian King", 1, "Deploy Barbarian");
        hero[1] = new Hero("Archer Queen", 1, "Deploy Archer");

        return hero;
    }

    // Encapsulation
    // Method untuk menghitung damage khusus yang dihasilkan oleh hero
    private int calculateSpecialDamage() {
        // Logika perhitungan damage khusus
        Random random = new Random();
        int baseDamage = getLevel() * 20;
        int randomFactor = random.nextInt(11) - 5; // Nilai random antara -5 sampai 5
        int totalDamage = baseDamage + randomFactor;
        
        return totalDamage;
    }

    public int getLevel() {
        return 1;
    }
}

// Abstraction
// Class Building yang merepresentasikan bangunan dalam permainan
class Building {
    // Encapsulation
    private String name;
    private int level;
    private int health;
    private boolean isDestroyed;
    
    // Constructor untuk membuat objek Building dengan nama dan level tertentu
    public Building(String name, int level) {
        this.name = name;
        this.level = level;
        health = 100 * level;
    }
    
    // Encapsulation
    // Getter untuk mengambil nama bangunan
    public String getName() {
        return name;
    }
    
    // Encapsulation
    // Method untuk mengambil bangunan yang dihancurkan
    public boolean isDestroyed() {
        return isDestroyed;
    }
    
    // Encapsulation
    // Method untuk menerima damage dari serangan pasukan
    public void takeDamage(int damage) {
        health -= damage;
        
        if (health <= 0) {
            isDestroyed = true;
            System.out.println("Bangunan " + name + " telah hancur!");
        } else {
            System.out.println("Bangunan " + name + " menerima damage sebesar " + damage + ".");
        }
    }
    
    // Encapsulation
    // Method untuk untuk proses penghancuran bangunan
    private void destroy() {
        isDestroyed = true;
        System.out.println(name + " telah dihancurkan.");
    }
}

// Abstraction
// Class DefenseBuilding yang merepresentasikan bangunan pertahanan dalam permainan
class DefenseBuilding extends Building {
    // Encapsulation
    private int attackRange;
    
    // Constructor untuk membuat objek DefenseBuilding dengan nama, level, dan attackRange tertentu
    public DefenseBuilding(String name, int level, int attackRange) {
        super(name, level);
        this.attackRange = attackRange;
    }
    
    // Encapsulation dan Polymorphism
    // Method untuk menyerang pasukan yang berada dalam jangkauan attackRange
    public void attackTroops(Troop[] troops) {
        System.out.println(getName() + " sedang menyerang pasukan...");
        
        // Menyerang pasukan yang berada dalam jangkauan attackRange
        for (Troop troop : troops) {
            if (isInRange(troop)) {
                takeDamage(troop.getLevel() * 5);
            }
        }
        
        System.out.println("Serangan selesai.");
    }
    
    // Encapsulation
    // Method untuk memeriksa apakah pasukan berada dalam jangkauan attackRange
    private boolean isInRange(Troop troop) {
        // Logika untuk memeriksa apakah pasukan berada dalam jangkauan attackRange
        return troop.getLevel() <= attackRange;
    }
}

class Main {
    public static void main(String[] args) {
        ClashOfClansUI coc = new ClashOfClansUI();

        try (Scanner scanner = new Scanner(System.in)) {
            boolean running = true;
            while (running) {
                System.out.println("!!! ======= Clash Of Clans ======= !!!");
                System.out.println("!! ======= Menu Registrasi  ======= !!");
                System.out.println("1. Login");
                System.out.println("2. Sign Up");
                System.out.println("3. Main Clash Of Clans");
                System.out.println("0. Exit");

                System.out.print("Masukkan pilihanmu: ");
                int choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        coc.login();
                        break;
                    case 2:
                        coc.signUp();
                        break;
                    case 3:
                        coc.playCoc();
                        break;
                    case 0:
                        running = false;
                        System.out.println("Terimakasih Telah Bermain Clash Of Clans. Sampai Jumpa!");
                        break;
                    default:
                        System.out.println("Pilihan Tidak Tersedia! Silahkan coba lagi.");
                        break;
                }

                System.out.println();
            }
        }
    }
}
